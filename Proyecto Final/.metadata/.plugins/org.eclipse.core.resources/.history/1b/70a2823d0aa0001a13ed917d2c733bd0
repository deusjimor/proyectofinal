package com.jcastro.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;
import com.jcastro.util.Util;

/**
 * Clase EnemigoEstatico
 * @author Jimor
 *
 */
public class EnemigoEstatico extends Enemigo{
	
	private BaseActor leftSensor;
	private BaseActor rightSensor;
	
	private boolean mirar;
	
	private Animation stand;
	private Animation ataque;
	private Animation ataque2;
	
	private float posX;
	private boolean atacando;
	
	private Sound soundBite;
	
	private long lastSound;
	
	public float soundVolume;

	/**
	 * Constructor de la clase enemigo estatico
	 * @param x
	 * @param y
	 * @param s
	 */
	public EnemigoEstatico(float x, float y, Stage s) {
		super(x, y, s);
		setName("Estatico");
		
		lastSound = System.currentTimeMillis();
		soundVolume = Util.volumenControlEffects;
		
		stand = loadAnimationFromSheet("assets/images/enemigos/plantaCarnivora.png", 1, 5, 0.1f, true);
		ataque = loadAnimationFromSheet("assets/images/enemigos/plantaCarnivoraAtaque.png", 1, 3, 0.1f, true);
		ataque2 = loadAnimationFromSheet("assets/images/enemigos/plantaCarnivoraAtaque2.png", 1, 3, 0.1f, true);
		
		mirar = false;
		posX = x;
		
		leftSensor = new BaseActor(0, 0, s);
		leftSensor.loadTexture("assets/images/white.png");
		leftSensor.setSize(150, getHeight() - 80);
		leftSensor.setBoundaryRectangle();
		leftSensor.setVisible(false);
		
		rightSensor = new BaseActor(0, 0, s);
		rightSensor.loadTexture("assets/images/white.png");
		rightSensor.setSize(150, getHeight() - 80);
		rightSensor.setBoundaryRectangle();
		rightSensor.setVisible(false);
		
		soundBite = (Sound)Gdx.audio.newSound(Gdx.files.internal("assets/music/bite.wav"));
		
	}
	
	/**
	 * Metodo act del enemigo estatico
	 */
	public void act(float dt) {
		super.act(dt);
		
		setX(posX);
		
		leftSensor.setPosition(getX() -160, getY() + 30);
		rightSensor.setPosition(getX() +80, getY() + 30);
		
		if (mirar) {
			setScaleX(1);
		} else {
			setScaleX(-1);
		}
		
		for (BaseActor actor : BaseActor.getList(getStage(), Tear.class.getName())) {
			Tear tear = (Tear) actor;
			if (leftSensor.overlaps(tear)) {
				leftSensor.setColor(Color.GREEN);
				setAnimation(ataque);
				atacando = true;
				setX(posX-100);
				
				musicControl();
				
			} else if (rightSensor.overlaps(tear)) {
				rightSensor.setColor(Color.GREEN);
				setAnimation(ataque);
				atacando = true;
				
				musicControl();
				
			} else {
				leftSensor.setColor(Color.RED);
				rightSensor.setColor(Color.RED);
				setAnimation(stand);
				atacando = false;
				
				
			}
		}
	}

	/**
	 * Metodo is para comprobar la direccion en la que mira
	 * @return
	 */
	public boolean isMirar() {
		return mirar;
	}

	/**
	 * Metodo set para establecer una direccion en la que mira
	 * @param mirar
	 */
	public void setMirar(boolean mirar) {
		this.mirar = mirar;
	}

	/**
	 * Metodo is para comprobar si esta atacando
	 * @return
	 */
	public boolean isAtacando() {
		return atacando;
	}

	/**
	 * Metodo set para establecer si ataca
	 * @param atacando
	 */
	public void setAtacando(boolean atacando) {
		this.atacando = atacando;
	}
	
	/**
	 * Metodo para controlar el sonido de los efectos
	 */
	public void musicControl() {
		long thisSound = System.currentTimeMillis();
		
		if (lastSound - thisSound < -300) {
			lastSound = thisSound;
			soundBite.play(soundVolume);	
		}
	}
	
	
	
	

}
