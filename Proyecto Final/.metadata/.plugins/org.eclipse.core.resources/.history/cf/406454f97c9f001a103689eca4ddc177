package com.jcastro.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AddAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.jcastro.actors.Coleccionables;
import com.jcastro.actors.Enemigo;
import com.jcastro.actors.EnemigoEstatico;
import com.jcastro.actors.EnemigoVolador;
import com.jcastro.actors.Fin;
import com.jcastro.actors.Plataforma;
import com.jcastro.actors.PlataformaFija;
import com.jcastro.actors.PlataformaTrampa;
import com.jcastro.actors.PlataformaTrampolin;
import com.jcastro.actors.PlataformaTrampolinNeon;
import com.jcastro.actors.PlataformaTranspasable;
import com.jcastro.actors.PlataformaTransportador;
import com.jcastro.actors.PuntuacionesEnPantalla;
import com.jcastro.actors.Solid;
import com.jcastro.actors.Tear;
import com.jcastro.actors.TinteGreen;
import com.jcastro.actors.TinteRojo;
import com.jcastro.actors.TinteYellow;
import com.jcastro.base.BaseActor;
import com.jcastro.base.BaseGame;
import com.jcastro.base.BaseScreen;
import com.jcastro.game.Game;
import com.jcastro.tiled.TilemapActor;
import com.jcastro.util.Util;

public class FourthLevel extends BaseScreen{
	private Tear tear;
	private boolean win;
	private boolean dead;
	
	private Label timerLife;
	
	private boolean vivo;
	private boolean parar;
	
	private int puntuacion;
	
	public Music superPowerCool;

	public static FourthLevel sharedInstanced;
	
	public float musicVolume;

	/**
	 * Metodo initialice del cuarto nivel, se carga el mapa de tiled, y se cargan todos los objetos que se referencian
	 */
	@Override
	protected void initialize() {
		sharedInstanced = this;
		
		vivo = false;
		parar = false;
		
		puntuacion = 0;
		musicVolume = Util.volumenControlMusic;
		
		superPowerCool = (Music)Gdx.audio.newMusic(Gdx.files.internal("assets/music/soundtracks/Super Power Cool Dude.mp3"));
		superPowerCool.setLooping(true);
		superPowerCool.setVolume(musicVolume);
		superPowerCool.play();
		
		TilemapActor tma = new TilemapActor("assets/tiled/map4.tmx", mainStage);
		
		BaseActor fondo = new BaseActor(0, 0, mainStage);
		fondo.loadTexture("assets/images/escenario/fondoNivel4.png");
		
		for (MapObject obj : tma.getRectangleList("Suelo")) {
			MapProperties props = obj.getProperties();
			new Solid((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("Muro")) {
			MapProperties props = obj.getProperties();
			new Solid((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("Techo")) {
			MapProperties props = obj.getProperties();
			new Solid((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("Fin")) {
			MapProperties props = obj.getProperties();
			new Fin((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("Plataforma")) {
			MapProperties props = obj.getProperties();
			new PlataformaFija((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("Trampolin")) {
			MapProperties props = obj.getProperties();
			new PlataformaTrampolinNeon((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("Trampa")) {
			MapProperties props = obj.getProperties();
			new PlataformaTrampa((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage,
					"neon");
		}
		
		for (MapObject obj : tma.getRectangleList("Transpasable")) {
			MapProperties props = obj.getProperties();
			new PlataformaTranspasable((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage,
					"neon");
		}
		int num = 1;
		for (MapObject obj : tma.getRectangleList("Teleport")) {
			MapProperties props = obj.getProperties();
			new PlataformaTransportador((float)props.get("x"), (float)props.get("y"), 
					(float)props.get("width"), (float)props.get("height"), mainStage, num++);
		}
		
		for (MapObject obj : tma.getRectangleList("Enemigo")) {
			MapProperties props = obj.getProperties();
			new EnemigoVolador((float)props.get("x"), (float)props.get("y"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("EnemigoEstatico")) {
			MapProperties props = obj.getProperties();
			new EnemigoEstatico((float)props.get("x"), (float)props.get("y"), mainStage);
		}
		
		for (MapObject obj : tma.getRectangleList("TinteRojo")) {
			MapProperties props = obj.getProperties();
			new TinteRojo((float)props.get("x"), (float)props.get("y"), mainStage, "neon");
		}
		
		for (MapObject obj : tma.getRectangleList("TinteGreen")) {
			MapProperties props = obj.getProperties();
			new TinteGreen((float)props.get("x"), (float)props.get("y"), mainStage, "neon");
		}
		
		for (MapObject obj : tma.getRectangleList("TinteYellow")) {
			MapProperties props = obj.getProperties();
			new TinteYellow((float)props.get("x"), (float)props.get("y"), mainStage, "neon");
		}
		
		MapObject startPoint = tma.getRectangleList("Start").get(0);
		MapProperties startProps = startPoint.getProperties();
		recargar(startProps);
		
		timerLife = new Label("", BaseGame.labelStyle);
		timerLife.setColor(Color.YELLOW);
		timerLife.setPosition(385, 360);
		uiStage.addActor(timerLife);

	}

	private void recargar(MapProperties startProps) {
		tear = new Tear((float)startProps.get("x"), (float)startProps.get("y"), mainStage);
		dead = false;
		win = false;
	}

	/**
	 * Metodo update, en el se comprobaran constantemente las colisiones, las acciones enemigas
	 * y si el player se mantiene con vida o gana
	 */
	@Override
	public void update(float dt) {
		colisiones();
		enemigoAccion();
		
		if (tear.getY() >= 7932) {
			tear.setPosition(tear.getX(), 7930);
		}
		
		if (dead) {
			superPowerCool.stop();
			if (!vivo) {
				tear.dead();
				vivo = true;
			}
			timerLife.setText("PRESS ANY KEY");
			if (Gdx.input.isKeyPressed(Keys.ANY_KEY)  && !Gdx.input.isKeyPressed(Keys.ESCAPE)) {
				Game.setActiveScreen(new FourthLevel());

			}
		}
		
		if (win) {
			superPowerCool.stop();
			Game.setActiveScreen(new WinScreen(puntuacion));
		}
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			backToMenu();
		}
				
	}

	/**
	 * Metodo colisiones, registra las colisiones del player con el entorno
	 */
	private void colisiones() {
		solidColisiones();
		colisionesEnemigo();
		
	}

	/**
	 * Metodo encargado de revisar las colisiones de los enemigos con el player
	 */
	private void colisionesEnemigo() {
		for (BaseActor enemigo : BaseActor.getList(mainStage, Enemigo.class.getName())) {
			if (enemigo.getName().equalsIgnoreCase("Volador")) {
				tear.preventOverlap(enemigo);
				if (tear.overlaps(enemigo)) {
					dead = true;
				}
			}
			if (enemigo.getName().equalsIgnoreCase("Estatico")) {
				EnemigoEstatico enEstatico = (EnemigoEstatico) enemigo;
				if (enEstatico.isAtacando() && tear.overlaps(enEstatico)) {
					dead = true;
				}
			}
		}
		
	}

	/**
	 * Metodo que comprueba si el usuario esta sobre objetos solidos
	 */
	private void solidColisiones() {
		for (BaseActor solid : BaseActor.getList(mainStage, Solid.class.getName())) {
			tear.preventOverlap(solid);
		}
		
		for (BaseActor plataforma : BaseActor.getList(mainStage, Plataforma.class.getName())) {
			if (plataforma.getName().equalsIgnoreCase("Fija")) {
				tear.preventOverlap(plataforma);
			}
			if (plataforma.getName().equalsIgnoreCase("Trampolin")) {
				tear.preventOverlap(plataforma);
				
				if (tear.belowOverlaps(plataforma) && tear.isFalling()) {
					tear.rebotar();
				
				}

			}
			if (plataforma.getName().equalsIgnoreCase("Transpasable") && tear.isJumping()) {
				//plataforma.setColor(Color.RED);
			} else if(plataforma.getName().equalsIgnoreCase("Transpasable") && !tear.isJumping()) {
				//plataforma.setColor(Color.GREEN);
				tear.preventOverlap(plataforma);

			}
			
			if (plataforma.getName().equalsIgnoreCase("Trampa")) {
				PlataformaTrampa plTrampa = (PlataformaTrampa) plataforma;
				tear.preventOverlap(plataforma);
				
				if (tear.belowOverlaps(plataforma) && !tear.getTipo().equalsIgnoreCase("fire")) {
					if (parar == false) {
						plTrampa.spikes(plataforma.getX(), plataforma.getY(), plataforma.getWidth(), plataforma.getHeight() + 28);
						parar = true;
					}
					dead = true;
				
				}	
			}
			
			if (plataforma.getName().equalsIgnoreCase("Teleport")) {
				PlataformaTransportador plTeleport = (PlataformaTransportador) plataforma;
				tear.preventOverlap(plataforma);
				
				if (tear.belowOverlaps(plataforma) && tear.isFalling()) {
					
					if (plTeleport.getDiferenciador() == 10) {
						tear.setPosition(475, 1900);
					}
					if (plTeleport.getDiferenciador() == 5) {
						tear.setPosition(850, 1450);
					}
					if (plTeleport.getDiferenciador() == 6) {
						tear.setPosition(150, 1650);
					}
					if (plTeleport.getDiferenciador() == 1) {
						tear.setPosition(475, 1450);
					}
					if (plTeleport.getDiferenciador() == 2) {
						tear.setPosition(850, 2100);
					}
					if (plTeleport.getDiferenciador() == 4) {
						tear.setPosition(150, 1450);
					}
					if (plTeleport.getDiferenciador() == 9) {
						tear.setPosition(850, 1650);
					}
					if (plTeleport.getDiferenciador() == 3) {
						tear.setPosition(250, 2100);
					}
					if (plTeleport.getDiferenciador() == 7) {
						tear.setPosition(475, 2450);
					}
					if (plTeleport.getDiferenciador() == 8) {
						tear.setPosition(475, 1100);
					}
					
				}
			}
			
		}
		
		for (BaseActor coleccionables : BaseActor.getList(mainStage, Coleccionables.class.getName())) {
			Coleccionables coleccionabe = (Coleccionables) coleccionables;
			if (tear.overlaps(coleccionables)) {
				coleccionabe.setRecogido(true);
				
				if (coleccionables.getName().equalsIgnoreCase("TinteRojo")) {
					tear.setTipo("fire");
					puntuacion += 100;
					new PuntuacionesEnPantalla(tear.getX(), tear.getY(), mainStage, 100, tear.getTipo());
				}
				if (coleccionables.getName().equalsIgnoreCase("TinteGreen")) {
					tear.setTipo("green");
					puntuacion += 100;
					new PuntuacionesEnPantalla(tear.getX(), tear.getY(), mainStage, 100, tear.getTipo());
				}
				if (coleccionables.getName().equalsIgnoreCase("TinteYellow")) {
					tear.setTipo("yellow");
					puntuacion += 100;
					new PuntuacionesEnPantalla(tear.getX(), tear.getY(), mainStage, 100, tear.getTipo());
				}
				if (coleccionables.getName().equalsIgnoreCase("Moneda")) {
					win = true;
					puntuacion += 500;
					new PuntuacionesEnPantalla(tear.getX(), tear.getY(), mainStage, 500, tear.getTipo());
					puntuacion += tear.getExtraPuntuation();
				}
				
			}
		}
		
		
	}
	
	/**
	 * Metodo para comprobar si los enemigos estan agresivos o no
	 */
	private void enemigoAccion() {
		for (BaseActor enemigo : BaseActor.getList(mainStage, Enemigo.class.getName())) {
			if (enemigo.getName().equalsIgnoreCase("Estatico")) {
				EnemigoEstatico enEstatico = (EnemigoEstatico) enemigo;
				if (tear.getX() < enEstatico.getX()) {
					enEstatico.setMirar(true);
				} else {
					enEstatico.setMirar(false);
				}
				
			}
				
		}
	}
	
	/**
	 * Metodo para termianr con el nivel si se vuelve al menu
	 */
	private void backToMenu() {
		superPowerCool.stop();
		Game.setActiveScreen(new LevelSeleccion());
	}


}
