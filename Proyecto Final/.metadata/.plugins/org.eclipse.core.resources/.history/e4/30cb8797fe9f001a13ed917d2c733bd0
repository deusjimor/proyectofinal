package com.jcastro.base;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Intersector.MinimumTranslationVector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Clase BaseActor
 * @author Jimor
 *
 */
public class BaseActor extends Group{
	
	private Animation<TextureRegion> animation;
	private float tiempoTranscurrido;
	private boolean animacionPausada;
	protected Vector2 velocityVec;
	protected Vector2 accelerationVec;
	private float acceleration;
	private float maxSpeed;
	private float deceleration;
	private Polygon boundaryPolygon;
	private static Rectangle worldBounds;
	
	/**
	 * Constructor de la clase BaseActor, le doy la posicion y el estado
	 * @param x
	 * @param y
	 * @param s
	 */
	public BaseActor(float x, float y, Stage s) {
		super();
		setPosition(x, y);
		s.addActor(this);
		
		animation = null;
		tiempoTranscurrido = 0;
		animacionPausada = false;
		velocityVec = new Vector2(0,0);
		accelerationVec = new Vector2(0,0);
		acceleration = 0;
		maxSpeed = 1000;
		deceleration = 0;
	}
	
	/**
	 * Metodo para configurar la animacion
	 * @param anim
	 */
	public void setAnimation(Animation<TextureRegion> anim) {
		animation = anim;
		TextureRegion tr = animation.getKeyFrame(0);
		float w = tr.getRegionWidth();
		float h = tr.getRegionHeight();
		setSize(w, h);
		setOrigin(w/2, h/2);
		
		if (boundaryPolygon == null) {
			setBoundaryRectangle();
		}
	}
	
	/**
	 * Metodo para la animacion en pausa
	 * @param pause
	 */
	public void setAnimationPaused(boolean pause) {
		animacionPausada = pause;
	}
	
	/**
	 * Metodo act sirve para actualizar constantemente el tiempo transcurrido
	 * a no ser que se encuentre en pausa, nos permitira poder actualizar las
	 * imagenes
	 */
	public void act(float dt) {
		super.act(dt);
		
		if(!animacionPausada) {
			tiempoTranscurrido += dt;
		}
	}
	
	/**
	 * Metodo para seleccionar la imagen correta que necesitare pintar segun el tiempo
	 * transcurrido, definire las posiciones, la escala, el tama�o, la rotacion y el origen
	 */
	public void draw(Batch batch, float parentAlpha) {

		Color c = getColor();
		batch.setColor(c.r, c.g, c.b, c.a);
		
		if (animation != null && isVisible()) {
			batch.draw(animation.getKeyFrame(tiempoTranscurrido),
					getX(), getY(), getOriginX(), getOriginY(),
					getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
		}
		
		super.draw(batch, parentAlpha);
	}
	
	/**
	 * Metodo para crear la animacion mediante archivos de imagen separados, este metodo
	 * devuelve una animacion
	 * @param fileNames
	 * @param frameDuration
	 * @param loop
	 * @return
	 */
	public Animation<TextureRegion> loadAnimationFromFiles(String[] fileNames, float frameDuration, boolean loop) {
		int fileCount = fileNames.length;
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		
		for (int i = 0; i < fileCount; i++) {
			String fileName = fileNames[i];
			Texture texture = new Texture(Gdx.files.internal(fileName));
			texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			textureArray.add(new TextureRegion(texture));
		}
		
		Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, textureArray);
		
		if (loop) {
			anim.setPlayMode(Animation.PlayMode.LOOP);
		} else {
			anim.setPlayMode(Animation.PlayMode.NORMAL);
		}
		
		if (animation == null) {
			setAnimation(anim);
		}
		
		return anim;
	}
	
	/**
	 * Metodo para crear una animacion mediante una hoja de sprites, gracias
	 * al metodo split de la clase TextureRegion, podemos dividir la hoja de
	 * sprites en subimagenes, para usarlo necesitamos conocer el tama�o de 
	 * cada subimagen y e numero de filas y columnas en la hoja de sprites
	 * 
	 * @param fileName
	 * @param rows
	 * @param cols
	 * @param frameDuration
	 * @param loop
	 * @return
	 */
	public Animation<TextureRegion> loadAnimationFromSheet(String fileName, int rows, int cols, float frameDuration,
			boolean loop) {
		Texture texture = new Texture(Gdx.files.internal(fileName), true);
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		int frameWidth = texture.getWidth() / cols;
		int frameHeigth = texture.getHeight() / rows;
		
		TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeigth);
		
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				textureArray.add(temp[r][c]);
			}
		}
		
		Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, textureArray);
		
		if (loop) {
			anim.setPlayMode(Animation.PlayMode.LOOP);
		} else {
			anim.setPlayMode(Animation.PlayMode.NORMAL);
		}
		
		if (animation == null) {
			setAnimation(anim);
		}
		
		return anim;
	}
	
	/**
	 * Metodo para cargar una imagen sin animacion, solo les dara un frame de animacion
	 * @param fileName
	 * @return
	 */
	public Animation<TextureRegion> loadTexture(String fileName) {
		String[] fileNames = new String[1];
		fileNames[0] = fileName;
		
		return loadAnimationFromFiles(fileNames, 1, true);
	}
	
	/**
	 * Metodo para comprobar si una animacion ha terminado, llamamos al metodo 
	 * isAnimationFinished, que comprobara si el loop de aimacion sigue vivo
	 * y si el tiempo transcurrido es mayor a el tiempo requerido
	 * @return
	 */
	public boolean isAnimationFinished() {
		return animation.isAnimationFinished(tiempoTranscurrido);
	}
	
	/**
	 * Metodo para establecer la velocidad del actor
	 * @param speed
	 */
	public void setSpeed(float speed) {
		if (velocityVec.len() == 0) {
			velocityVec.set(speed, 0);
		} else {
			velocityVec.setLength(speed);
		}
	}
	
	/**
	 * Metodo que me retorna la velociad del actor;
	 * @return
	 */
	public float getSpeed() {
		return velocityVec.len();
	}
	
	/**
	 * 	Metodo para darle el angulo de direccion al actor
	 * @param angle
	 */
	public void setMotionAngle(float angle) {
		velocityVec.setAngle(angle);
	}
	
	/**
	 * Metodo para obtener el angulo de movimiento del actor, nos permitara
	 * poder orientar el objeto en la direccion en la que se esta moviendo
	 * @return
	 */
	public float getMotionAngle() {
		return velocityVec.angle();
	}
	
	/**
	 * Metodo para saber si el objeto esta en movimiento o no
	 * @return
	 */
	public boolean isMoving() {
		return (getSpeed() > 0);
	}
	
	/**
	 * Metodo para establecer la acceleracion
	 * @param acc
	 */
	public void setAcceleration(float acc) {
		acceleration = acc;
	}
	
	/**
	 * Metodo para aceelerar el objeto segun el angulo
	 * @param angle
	 */
	public void accelerateAtAngle(float angle) {
		accelerationVec.add(new Vector2(acceleration, 0).setAngle(angle));
	}
	
	/**
	 * Metodo para accelerar un objeto en la direccion en la que se esta moviendo
	 */
	public void accelerateForward() {
		accelerateAtAngle(getRotation());
	}
	
	/**
	 * Metodo para establecer la velocidad maxima del objeto
	 * @param ms
	 */
	public void setMaxSpeed(float ms) {
		maxSpeed = ms;
	}
	
	/**
	 * Metodo para establecer la desaceleracion de un objeto
	 * @param dece
	 */
	public void setDeceleration(float dece) {
		deceleration = dece;
	}
	
	/**
	 * Este metodo toma el tiempo transcurrido desde la ultima actiualizacion
	 * y aplica las siguientes funciones
	 * @param dt
	 */	
	public void apllyPhysics(float dt) {
		//	Aplicar aceleracion
		velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		
		float speed = getSpeed();
		
		//	Reducir la velocidad cuando no este acelerando 
		if (accelerationVec.len() == 0) {
			speed -= deceleration * dt;
		}
		
		//	Mantener la velocidad en los limites
		speed = MathUtils.clamp(speed, 0, maxSpeed);
		
		//	Actualizar velocidad
		setSpeed(speed);
		
		//	Aplicar velocidad
		moveBy(velocityVec.x * dt, velocityVec.y * dt);
		
		//	Reiniciar velociad
		accelerationVec.set(0,0);
	}
	
	/**
	 * Metodo para establecer el perimetro en forma de rectangulo
	 */
	public void setBoundaryRectangle() {
		float w = getWidth();
		float h = getHeight();
		float[]	vertices = {0,0, w,0, w,h, 0,h};
		boundaryPolygon = new Polygon(vertices);
	}
	
	/**
	 * Metodo para establecer el perimetro en forma de poligono
	 * @param numSides
	 */
	public void setBoundaryPolygon(int numSides) {
		float w = getWidth();
		float h = getHeight();
		
		float[] vertices = new float[2*numSides];
		for (int i = 0; i < numSides; i++) {
			float angle = i * 6.28f / numSides;
			
			//	coordeanda X
			vertices[2*i] = w/2 * MathUtils.cos(angle) + w/2;
			
			//	coordenada y
			vertices[2*i+1] = h/2 * MathUtils.sin(angle) + h/2;
		}
		
		boundaryPolygon = new Polygon(vertices);
	}
	
	/**
	 * Metodo para establecer el perimetro en forma de poligono de tamanyo reducido
	 * @param numSides
	 */
	public void setBoundaryPolygonReducido(int numSides) {
		float w = getWidth();
		float h = getHeight();
		
		float[] vertices = new float[2*numSides];
		for (int i = 0; i < numSides; i++) {
			float angle = i * 6.28f / numSides;
			
			//	coordeanda X
			vertices[2*i] = w/2 * MathUtils.cos(angle) + w/4;
			
			//	coordenada y
			vertices[2*i+1] = h/2 * MathUtils.sin(angle) + h/2;
		}
		
		boundaryPolygon = new Polygon(vertices);
	}
	
	/**
	 * Metodo para retornar el poligono de deteccion del objeto
	 * @return
	 */
	public Polygon getBoundaryPolygon() {
		boundaryPolygon.setPosition(getX(), getY());
		boundaryPolygon.setOrigin(getOriginX(), getOriginY());
		boundaryPolygon.setRotation(getRotation());
		boundaryPolygon.setScale(getScaleX(), getScaleY());
		
		return boundaryPolygon;
	}
	
	/**
	 * Metodo para comprobar las colisiones de los poligonos
	 * @param other
	 * @return
	 */
	public boolean overlaps(BaseActor other) {
		Polygon poly1 = this.getBoundaryPolygon();
		Polygon poly2 = other.getBoundaryPolygon();
		
		//	Con esta comprobacion comprobamos si los rectangulos que rodean los poligonos se cruzan
		//	si no es asi inmediatamente retorna un false y evita la comprobacion de calculos
		if (!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())) {
			return false;
		}
		
		return Intersector.overlapConvexPolygons(poly1, poly2);
	}
	
	//	Este metodo centra la posicion en el medio del objeto
	public void centerAtPosition(float x, float y) {
		setPosition(x-getWidth()/2, y-getHeight()/2);
	}
	
	//	Este metodo centra usara el metodo para centrar la poscion del actor
	public void centerAtActor(BaseActor other) {
		centerAtPosition(other.getX() + other.getWidth()/2, other.getY() + other.getHeight()/2);
	}
	
	//	Metodo que cambia el canal alfa del color para agregar Opacidad
	public void setOpacity(float opacity) {
		this.getColor().a = opacity;
	}
	
	//	Metodo que comprueba la colision, si la colision es afirmativa mueve el objeto a la posicion
	//	anterior
	public Vector2 preventOverlap(BaseActor other) {
		Polygon poly1 = this.getBoundaryPolygon();
		Polygon poly2 = other.getBoundaryPolygon();
		
		//	Con esta comprobacion comprobamos si los rectangulos que rodean los poligonos se cruzan
		//	si no es asi inmediatamente retorna un false y evita la comprobacion de calculos
		if (!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())) {
			return null;
		}
		
		MinimumTranslationVector mtv = new MinimumTranslationVector();
		boolean polygonOverlap = Intersector.overlapConvexPolygons(poly1, poly2, mtv);
		
		if (!polygonOverlap) {
			return null;
		}
		
		this.moveBy(mtv.normal.x * mtv.depth, mtv.normal.y * mtv.depth);
		return mtv.normal;
	}
	
	//	Metodo para crear listas de tipo de objeto que requiramos
	public static ArrayList<BaseActor> getList(Stage stage, String className) {
		ArrayList<BaseActor> list = new ArrayList<BaseActor>();
		
		@SuppressWarnings("rawtypes")
		Class theClass = null;
		
		try {
			theClass = Class.forName(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		for (Actor a : stage.getActors()) {
			if (theClass.isInstance(a)) {
				list.add((BaseActor) a);
			}
		}
		
		return list;
	}
	
	//	Metodo que devuelve la cantidad de objetos en la lista de objetos del
	//	tipo especificado
	public static int count(Stage stage, String className) {
		return getList(stage, className).size();
	}
	
	//	Metodo para almacenar el tama�o del juego mediante valores
	public static void setWorldBounds(float width, float height) {
		worldBounds = new Rectangle(0, 0, width, height);
	}
	
	//	Metodo para almacenar el tama�o del juego mediante un actor
	public static void setWorldBounds(BaseActor ba) {
		setWorldBounds(ba.getWidth(), ba.getHeight());
	}
	
	//	Metodo apra controlar la posicion del actor dentro de los margenes
	//	del mundo
	public void boundToWorld() {
		
		//	Borde izquierdo
		if (getX() < 25) {
			setX(0 + 25);
		}
		
		//	Borde derecho
		if (getX() + (getWidth() + 25) > worldBounds.width ) {
			setX(worldBounds.width - (getWidth() + 25));
		}
		
		// Borde inferior
		if (getY() < 0 + 40) {
			setY(0 + 40);
		}
		
		// Borde superior
		if (getY() + getHeight() + 10 > worldBounds.height) {
			setY(worldBounds.height - getHeight() - 10);
		}
	}
	
	//	Metodo para alinear la camara con el actor
	public void alignCamera() {
		Camera cam = this.getStage().getCamera();
		Viewport v = this.getStage().getViewport();
		
		//	Centrar camara en el actor
		cam.position.set(this.getX() + this.getOriginX(), this.getY() + this.getOriginY(), 0);
		
		//	Camara fijada al dise�o
		cam.position.x = MathUtils.clamp(cam.position.x, cam.viewportWidth/2, worldBounds.width - cam.viewportHeight/2);
		cam.position.y = MathUtils.clamp(cam.position.y, cam.viewportHeight/2, worldBounds.height - cam.viewportHeight/2);
		cam.update();
	}
	
	public void alignCameraEjeXFijo() {
		Camera cam = this.getStage().getCamera();
		Viewport v = this.getStage().getViewport();
		
		//	Centrar camara en el actor
		cam.position.set(480, this.getY() + this.getOriginY(), 0);
		
		//	Camara fijada al dise�o
		//cam.position.x = MathUtils.clamp(cam.position.x, cam.viewportWidth/2, worldBounds.width - cam.viewportHeight/2);
		cam.position.y = MathUtils.clamp(cam.position.y, cam.viewportHeight/2, worldBounds.height - cam.viewportHeight/2);
		cam.update();
	}
	
	//	Metodo para comprobar la distancia 
	public boolean isWithinDistance(float distance, BaseActor other) {
        Polygon polygon1 = this.getBoundaryPolygon();
        float scaleX = (this.getWidth() + 2 * distance) / this.getWidth();
        float scaleY = (this.getHeight() + 2 * distance) / this.getHeight();
        polygon1.setScale(scaleX, scaleY);

        Polygon polygon2 = other.getBoundaryPolygon();
        if(!polygon1.getBoundingRectangle().overlaps(polygon2.getBoundingRectangle())) {
            return false;
        }

        return Intersector.overlapConvexPolygons(polygon1, polygon2);
    }
}


