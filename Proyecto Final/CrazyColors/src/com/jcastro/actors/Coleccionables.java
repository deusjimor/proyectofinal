package com.jcastro.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;

/**
 * Clase Coleccionables
 * @author Jimor
 *
 */
public class Coleccionables extends BaseActor{
	
	private boolean recogido;

	/**
	 * Constructor de la calse Coleccionables
	 * @param x
	 * @param y
	 * @param s
	 */
	public Coleccionables(float x, float y, Stage s) {
		super(x, y, s);
		
		recogido = false;
		
	}
	
	/**
	 * Metodo act de la clase Coleccionables
	 */
	public void act(float dt) {
		super.act(dt);
		
		if (recogido == true) {
			this.remove();
		}
	}

	/**
	 * Metodo is para comprobar si se ha recogido el coleccionable
	 * @return
	 */
	public boolean isRecogido() {
		return recogido;
	}
	
	/**
	 * Metodo set para cambiar si es recogido o no
	 * @param recogido
	 */
	public void setRecogido(boolean recogido) {
		this.recogido = recogido;
	}
	
	

}
