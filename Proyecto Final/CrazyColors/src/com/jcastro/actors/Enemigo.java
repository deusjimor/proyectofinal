package com.jcastro.actors;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;

/**
 * Clase Enemigo
 * @author Jimor
 *
 */
public class Enemigo extends BaseActor{
	
	private boolean enable;

	/**
	 * Constructor de la clase Enemigo
	 * @param x
	 * @param y
	 * @param s
	 */
	public Enemigo(float x, float y, Stage s) {
		super(x, y, s);
		
		enable = true;
	}
	
	/**
	 * Metodo act de la clase enemigo
	 */
	public void act(float dt) {
		super.act(dt);
		
		boundToWorld();
	}

	/**
	 * Metodo is para comprobar si el enemigo esta activado
	 * @return
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * Metodo set del enemigo para establecer si esta activado
	 * @param enable
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

}
