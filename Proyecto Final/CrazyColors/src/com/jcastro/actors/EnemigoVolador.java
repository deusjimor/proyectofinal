package com.jcastro.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;

/**
 * Clase EnemigoVolador
 * @author Jimor
 *
 */
public class EnemigoVolador extends Enemigo{
	
	private Animation movimiento;
	private float walkAcceleration;
	private float maxHorizontalSpeed;
	
	private BaseActor leftSensor;
	private BaseActor rightSensor;
	private boolean derecha;
	private boolean izquierda;

	/**
	 * Constructor de la clase EnemigoVolador, le da una anumacion por sheet, carga sus sensores,
	 * inicializa sus variables y le da una hitbox
	 * @param x
	 * @param y
	 * @param s
	 */
	public EnemigoVolador(float x, float y, Stage s) {
		super(x, y, s);
		setName("Volador");
		
		movimiento = loadAnimationFromSheet("assets/images/enemigos/monstruo.png", 1, 4, 0.1f, true);
		
		walkAcceleration = 1000;
		maxHorizontalSpeed = 200;
		
		setBoundaryPolygon(8);
		
		leftSensor = new BaseActor(0, 0, s);
		leftSensor.loadTexture("assets/images/white.png");
		leftSensor.setSize(5, getHeight());
		leftSensor.setBoundaryRectangle();
		leftSensor.setVisible(false);
		
		rightSensor = new BaseActor(0, 0, s);
		rightSensor.loadTexture("assets/images/white.png");
		rightSensor.setSize(5, getHeight());
		rightSensor.setBoundaryRectangle();
		rightSensor.setVisible(false);
		
		izquierda = true;
		derecha = false;
		
	}
	
	/**
	 * Metodo act de la clase enemigo volador
	 */
	public void act(float dt) {
		super.act(dt);
	
		leftSensor.setPosition(getX() -3, getY());
		rightSensor.setPosition(getX() +45, getY());
		
		if (izquierda == true) {
			accelerationVec.add(-walkAcceleration, 0 );
		} else {
			accelerationVec.add(walkAcceleration, 0 );
		}
		
		velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );
		velocityVec.x = MathUtils.clamp( velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed );
		
		moveBy( velocityVec.x * dt, velocityVec.y * dt );
		accelerationVec.set(0,0);
		
		if (this.leftIsOnSolid()) {
			leftSensor.setColor(Color.GREEN);
			izquierda = false;
			derecha = true;
			
		} else if (this.rightIsOnSolid()) {
			rightSensor.setColor(Color.GREEN);
			izquierda = true;
			derecha = false;
			
		} else {
			leftSensor.setColor(Color.RED);
			rightSensor.setColor(Color.RED);
			
		}
		
		if ( velocityVec.x > 0 ) {
			setScaleX(-1);
		}
		if ( velocityVec.x < 0 ) {
			setScaleX(1);
		}
		
	}
	
	/**
	 * Metodo para comprobar si el sensor de la izquierda colisiona con algun elemento solido o una plataforma
	 * @return
	 */
	public boolean leftIsOnSolid() {
		for (BaseActor actor : BaseActor.getList(getStage(), Plataforma.class.getName())) {
			Plataforma plataforma = (Plataforma)actor;
			
			if (leftOverlaps(plataforma) && plataforma.isEnable()) {
				return true;
			}
		}
		
		for (BaseActor actor : BaseActor.getList(getStage(), Solid.class.getName())) {
			Solid solid = (Solid)actor;
			
			if (leftOverlaps(solid) && solid.isEnable()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Metodo para comprobar si el sensor de la derecha colisiona con algun elemento solido o una plataforma
	 * @return
	 */
	public boolean rightIsOnSolid() {
		for (BaseActor actor : BaseActor.getList(getStage(), Plataforma.class.getName())) {
			Plataforma plataforma = (Plataforma)actor;

			if (rightOverlaps(plataforma) && plataforma.isEnable()) {
				return true;
			}
		}
		
		for (BaseActor actor : BaseActor.getList(getStage(), Solid.class.getName())) {
			Solid solid = (Solid)actor;

			if (rightOverlaps(solid) && solid.isEnable()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Metodo para comprobar si el sensor izquierdo colisiona con el objeto que le pasamos
	 * @param actor
	 * @return
	 */
	public boolean leftOverlaps(BaseActor actor) {
        return leftSensor.overlaps(actor);
    }
	
	/**
	 * Metodo para comprobar si el sensor derecho colisiona con el objeto que le pasamos
	 * @param actor
	 * @return
	 */
	public boolean rightOverlaps(BaseActor actor) {
        return rightSensor.overlaps(actor);
    }

}
