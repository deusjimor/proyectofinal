package com.jcastro.actors;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;

/**
 * Clase Fin
 * @author Jimor
 *
 */
public class Fin extends Coleccionables{

	/**
	 * Construcot de la clase, carga su animacion por sheet, le da tama�o y una hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 */
	public Fin(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		setName("Moneda");
	
		loadAnimationFromSheet("assets/images/coleccionables/moneda.png", 1, 6, 0.1f, true);
		setSize(width, height);
		setBoundaryRectangle();
	
	}
	
}
