package com.jcastro.actors;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.jcastro.base.BaseActor;

/**
 * Clase ManchaDePintura
 * @author Jimor
 *
 */
public class ManchaDePintura extends BaseActor{

	/**
	 * Constructor de la clase manchaDePintura, carga su textura, le carga un color y una dimension
	 * dependiendo del color del player
	 * @param x
	 * @param y
	 * @param s
	 * @param color
	 */
	public ManchaDePintura(float x, float y, Stage s, String color) {
		super(x, y, s);
		
		loadTexture("assets/images/pintura.png");
		if (color.equalsIgnoreCase("red")) {
			setColor(Color.RED);
			randomDimension();
			//setOpacity(99);
		} else if (color.equalsIgnoreCase("green")) {
			setColor(Color.GREEN);
			randomDimension();
			//setOpacity(99);
		} else if (color.equalsIgnoreCase("yellow")) {
			setColor(Color.YELLOW);
			randomDimension();
			//setOpacity(99);
		}
		
		addAction(Actions.fadeOut(5.0f));
		addAction(Actions.after(Actions.removeActor(this)));
	}
	
	/**
	 * Metodo para dar tamanyo a la sprite
	 */
	public void randomDimension() {
		float num = setSizeForFall();
		setScale(num, num);
	}
	
	/**
	 * Metodo que calcula la velocidad de caida del player para dar una dimension
	 * @return
	 */
	public float setSizeForFall() {
		if (Tear.sharedInstanced.getVelocityY() == -900) {
			return 1.5f;
		} else if (Tear.sharedInstanced.getVelocityY() > -900 && Tear.sharedInstanced.getVelocityY() <= -600) {
			return 1.0f;
		} else if (Tear.sharedInstanced.getVelocityY() > -600 && Tear.sharedInstanced.getVelocityY() <= -400) {
			return 0.8f;
		} else if (Tear.sharedInstanced.getVelocityY() > -400 && Tear.sharedInstanced.getVelocityY() <= -200) {
			return 0.6f;
		} else if (Tear.sharedInstanced.getVelocityY() > -200 && Tear.sharedInstanced.getVelocityY() <= 0) {
			return 0.4f;
		} 
		
		return (float)0.5;
	}
	
	/**
	 * Metodo para devolver un random
	 * @return
	 */
	public float random() {
		float num = (float) (Math.random()*1);
		while (num < 0.5) {
			num = (float) (Math.random()*1);
		}
		return num;
	}

}
