package com.jcastro.actors;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;

/**
 * Clase Plataforma
 * @author Jimor
 *
 */
public class Plataforma extends BaseActor{
	
	private boolean enable;

	/**
	 * Constructor de la clase Plataforma
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 */
	public Plataforma(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		
		
		enable = true;
	}

	/**
	 * Metodo is que comprueba si la plataforma esta activada o desactivada
	 * @return
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * Metodo set para establecer la plataforma como activada o desactivada
	 * @param enable
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

}
