package com.jcastro.actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase PlataformaFija
 * @author Jimor
 *
 */
public class PlataformaFija extends Plataforma{

	/**
	 * Constructor de la clase PlataformaFija, carga su tamanyo y le da hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 */
	public PlataformaFija(float x, float y, float width, float height, Stage s) {
		super(x, y, width, height, s);
		setName("Fija");
		
		//loadTexture("assets/images/Solid.png");
		setSize(width, height);
		setBoundaryRectangle();
		
	}

}
