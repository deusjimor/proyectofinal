package com.jcastro.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase PlataformaTrampa
 * @author Jimor
 *
 */
public class PlataformaTrampa extends Plataforma{
	
	private Animation spikesUp;

	/**
	 * Constructor de la clase paltaformaTrampa, carga sus animaciones y selecciona el tipo de animacion
	 * dependiendo del tipo de mapa, le da un tamanyo y le da una hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 * @param version
	 */
	public PlataformaTrampa(float x, float y, float width, float height, Stage s, String version) {
		super(x, y, width, height, s);
		setName("Trampa");
		
		if (version.equalsIgnoreCase("normal")) {
			loadAnimationFromSheet("assets/images/escenario/plataformas/spikes.png", 1, 1, 01.f, true);

		} else if (version.equalsIgnoreCase("neon")) {
			loadAnimationFromSheet("assets/images/escenario/plataformas/neonSpikes.png", 1, 1, 01.f, true);
		}
		setSize(width, height);
		setBoundaryRectangle();
		
		if (version.equalsIgnoreCase("normal")) {
			spikesUp = loadAnimationFromSheet("assets/images/escenario/plataformas/spikesAnimation.png", 1, 4, 0.1f, false);

		} else if (version.equalsIgnoreCase("neon")) {
			spikesUp = loadAnimationFromSheet("assets/images/escenario/plataformas/neonSpikesAnimation.png", 1, 4, 0.1f, false);
		}
		
	}
	
	/**
	 * Metodo para cambiar la animacion
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void spikes(float x, float y, float width, float height) {
		
		setAnimation(spikesUp);
		setSize(width, height);
	}

}
