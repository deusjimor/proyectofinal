package com.jcastro.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase PlataformaTrampolin
 * @author Jimor
 *
 */
public class PlataformaTrampolin extends Plataforma{

	/**
	 * Constructor de la clase PlataformaTrampolin, carga su textura le da un tamanyo
	 * y le asigna una hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 */
	public PlataformaTrampolin(float x, float y, float width, float height, Stage s) {
		super(x, y+5, width, height, s);
		setName("Trampolin");
		
		loadTexture("assets/images/escenario/plataformas/plataformaTrampolin.png");
		setSize(width, height);
		setBoundaryRectangle();
		
		
	}

	
	
	

}
