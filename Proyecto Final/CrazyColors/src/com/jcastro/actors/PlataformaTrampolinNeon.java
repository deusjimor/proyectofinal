package com.jcastro.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase PlataformaTrampolinNeon
 * @author Jimor
 *
 */
public class PlataformaTrampolinNeon extends Plataforma{

	/**
	 * Construcotr de la clase Plataforma Trampolin Neon, se le asigna una animacion,
	 * un tamanyo y se le da una hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 */
	public PlataformaTrampolinNeon(float x, float y, float width, float height, Stage s) {
		super(x, y, width, height, s);
		setName("Trampolin");
		
		loadAnimationFromSheet("assets/images/escenario/plataformas/plataformaTrampolinTecnoSheet.png", 1, 9, 0.1f, true);
		setSize(width, height);
		setBoundaryRectangle();
		
		
	}

	
	
	

}
