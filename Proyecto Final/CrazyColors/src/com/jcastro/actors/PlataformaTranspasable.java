package com.jcastro.actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase PlataformaTranspasables
 * @author Jimor
 *
 */
public class PlataformaTranspasable extends Plataforma{

	/**
	 * Constructor de la clase PlataformaTranspasable, se le asigna una textura o una animacion
	 * dependiendo del estilo del nivel, se le asigna un tama�o y una hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 * @param version
	 */
	public PlataformaTranspasable(float x, float y, float width, float height, Stage s, String version) {
		super(x, y-20, width, height, s);
		setName("Transpasable");
		
		if (version.equalsIgnoreCase("normal")) {
			loadTexture("assets/images/escenario/plataformas/plataformaTranspasable.png");
		} else if (version.equalsIgnoreCase("neon")) {
			loadAnimationFromSheet("assets/images/escenario/plataformas/neonPlataformaTranspasableSheet.png", 1, 4, 0.1f, true);
		}
		
		setSize(width, height+20);
		setBoundaryRectangle();
	}

}
