package com.jcastro.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase Plataforma Transportador
 * @author Jimor
 *
 */
public class PlataformaTransportador extends Plataforma{
	
	int diferenciador;
	
	/**
	 * Constructor de la clase PlataformaTransportador, se le asigna una animacion, un tamanyo y una hitbox
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 * @param num
	 */
	public PlataformaTransportador(float x, float y, float width, float height, Stage s, int num) {
		super(x, y, width, height, s);
		setName("Teleport");
		
		diferenciador = num;
		
		loadAnimationFromSheet("assets/images/escenario/plataformas/plataformaTransportTecnoSheet.png", 1, 9, 0.1f, true);
		setSize(width, height);
		setBoundaryRectangle();
	}

	/**
	 * Metodo getter para tomar su identificador
	 * @return
	 */
	public int getDiferenciador() {
		return diferenciador;
	}

	/**
	 * Metodo setter para darle un identificador
	 * @param diferenciador
	 */
	public void setDiferenciador(int diferenciador) {
		this.diferenciador = diferenciador;
	}
	
	

	
	
	

}
