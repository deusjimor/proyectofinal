package com.jcastro.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.jcastro.base.BaseActor;

/**
 * clase PuntuacionesEnPantalla
 * @author Jimor
 *
 */
public class PuntuacionesEnPantalla extends BaseActor{

	/**
	 * Constructor de la clase puntuaciones en pantalla, se le asigna una textura, un color, un tamanyo
	 * y se le dan las acciones de que ascienda y desaparezca al rato
	 * @param x
	 * @param y
	 * @param s
	 * @param puntuacion
	 * @param color
	 */
	public PuntuacionesEnPantalla(float x, float y, Stage s, int puntuacion, String color) {
		super(x-110, y+20, s);
		
		selectTexture(puntuacion);
		selectColor(color);
		
		setScale(0.3f, 0.3f);
		
		addAction(Actions.moveBy(0, 100, 3f));
		addAction(Actions.fadeOut(5.0f));
		addAction(Actions.after(Actions.removeActor(this)));
	}

	/**
	 * Metodo para seleccionar la textura dependiendo de la puntuacion que el player saque
	 * @param puntuacion
	 */
	private void selectTexture(int puntuacion) {
		switch (puntuacion) {
		case 50:
			loadTexture("assets/images/puntos/50.png");
			break;
		case 100:
			loadTexture("assets/images/puntos/100.png");
			break;
		case 200:
			loadTexture("assets/images/puntos/200.png");
			break;
		case 500:
			loadTexture("assets/images/puntos/500.png");
			break;
		case -10:
			loadTexture("assets/images/puntos/-10.png");
			break;
		case -50:
			loadTexture("assets/images/puntos/-50.png");
			break;
		}
	}
	
	/**
	 * Metodo para seleccionar un color dependiendo del color del player
	 * @param color
	 */
	private void selectColor(String color) {
		switch (color) {
		case "red":
			setColor(Color.RED);
			break;
		case "green":
			setColor(Color.GREEN);
			break;
		case "yellow":
			setColor(Color.YELLOW);
			break;
		case "base":
			setColor(null);
			break;
		}
	}

}
