package com.jcastro.actors;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jcastro.base.BaseActor;

/**
 * Clase Solid
 * @author Jimor
 *
 */
public class Solid extends BaseActor{
	
	private boolean enable;

	/**
	 * Construcotr de la clase solid, se le da un tamanyo, una hitbox y se inicializan sus variables
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param s
	 */
	public Solid(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		
		//loadTexture("assets/images/Solid.png");
		setSize(width, height);
		setBoundaryRectangle();
		enable = true;
	}

	/**
	 * Metodo is para comprobar si el objeto esta activada o desactivada
	 * @return
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * Metodo setterpara establecer si la plataforma esta activada o no
	 * @param enable
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	

}
