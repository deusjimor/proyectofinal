package com.jcastro.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.jcastro.base.BaseActor;
import com.jcastro.game.Game;
import com.jcastro.util.Util;
import com.jcastro.windows.FirstLevel;
import com.jcastro.windows.Menu;

/**
 * Clase Tear
 * @author Jimor
 *
 */
public class Tear extends BaseActor{

	private Animation quieto;
	private Animation fluir;
	
	private float walkAcceleration;
	private float walkDeceleration;
	private float maxHorizontalSpeed;
	private float gravity;
	private float maxVerticalSpeed;
	
	private int extraPuntuation;
	
	private float velocityY;
	
	private Animation jump;
	private float jumpSpeed;
	private BaseActor belowSensor;
	
	private Animation dead;
	
	private boolean vivo;
	private String tipo;
	
	private Animation quietoFire;
	private Animation fluirFire;
	private Animation jumpFire;
	
	private Animation quietoJumper;
	private Animation fluirJumper;
	private Animation jumpJumper;
	
	private Animation quietoBubble;
	private Animation fluirBubble;
	private Animation jumpBubble;
	
	private Sound soundJump;
	private Sound soundJumpGreen;
	private Sound soundRebotar;
	private Sound soundPintura;
	
	public static Tear sharedInstanced;
	
	public float soundsVolume;
	
	boolean colisionando = false;
	
	/**
	 * Constructor de la clase Tear o player, se inicializan sus variables, se cargan sus animaciones,
	 * se le asigna una hitbox y una animacion incial, alinea su camara y le limita a los bordes dle mundo
	 * @param x
	 * @param y
	 * @param s
	 */
	public Tear(float x, float y, Stage s) {
		super(x, y, s);
		sharedInstanced = this;
		
		extraPuntuation = 0;
		
		soundsVolume = Util.volumenControlEffects;
		
		jump = loadAnimationFromSheet("assets/images/slime/base/slimeJump.png", 1, 8, 0.1f, true);
		quieto = loadAnimationFromSheet("assets/images/slime/base/slimeBase.png", 1, 5, 0.1f, true);
		fluir = loadAnimationFromSheet("assets/images/slime/base/slimeFluir.png", 1, 8, 0.1f, true);
		dead = loadAnimationFromSheet("assets/images/slime/base/slimeMuerte.png", 1, 2, 0.1f, false);
		
		tipo = "base";
		
		maxHorizontalSpeed = 200;
		walkAcceleration = 1000;
		walkDeceleration = 1000;
		gravity = 800;
		maxVerticalSpeed = 1000;
		
		setBoundaryPolygon(8);
		
		jumpSpeed = 500;
		
		//	Sensor
		belowSensor = new BaseActor(0, 0, s);
		belowSensor.loadTexture("assets/images/white.png");
		belowSensor.setSize(this.getWidth() - 8, 8);
		belowSensor.setBoundaryRectangle();
		belowSensor.setVisible(false);
		vivo = true;
		
		jumpFire = loadAnimationFromSheet("assets/images/slime/fire/flameSlimeJump.png", 1, 8, 0.1f, true);
		quietoFire = loadAnimationFromSheet("assets/images/slime/fire/flameSlimeBase.png", 1, 5, 0.1f, true);
		fluirFire = loadAnimationFromSheet("assets/images/slime/fire/flameSlimeFluir.png", 1, 8, 0.1f, true);
		
		quietoJumper = loadAnimationFromSheet("assets/images/slime/jumper/slimebaseJumper.png", 1, 5, 0.1f, true);
		fluirJumper = loadAnimationFromSheet("assets/images/slime/jumper/slimeFluirJumper.png", 1, 8, 0.1f, true);
		jumpJumper = loadAnimationFromSheet("assets/images/slime/jumper/slimeJumpJumper.png", 1, 8, 0.1f, true);
		
		jumpBubble = loadAnimationFromSheet("assets/images/slime/bubble/slimeJumpBubble.png", 1, 8, 0.1f, true);
		quietoBubble = loadAnimationFromSheet("assets/images/slime/bubble/slimeBaseBubble.png", 1, 5, 0.1f, true);
		fluirBubble = loadAnimationFromSheet("assets/images/slime/bubble/slimeFluirBubble.png", 1, 8, 0.1f, true);
		
		soundJump = (Sound)Gdx.audio.newSound(Gdx.files.internal("assets/music/jump.wav"));
		soundJumpGreen = (Sound)Gdx.audio.newSound(Gdx.files.internal("assets/music/jumpGreen.wav"));
		soundRebotar = (Sound)Gdx.audio.newSound(Gdx.files.internal("assets/music/rebotar.mp3"));
		soundPintura  = (Sound)Gdx.audio.newSound(Gdx.files.internal("assets/music/pop.wav"));
		
	}
	
	/**
	 * Metodo act de la clase tear, en ella se calculan sus aceleraciones sobre los ejes x y, 
	 * tambien asigna una animacion dependiendo de los coleccionalbes que adquiera
	 */
	public void act(float dt) {
		super.act( dt );
		
		if (vivo == true) {
			if (Gdx.input.isKeyPressed(Keys.A)) {
				 accelerationVec.add( -walkAcceleration, 0 );
			}
				
			if (Gdx.input.isKeyPressed(Keys.D)) {
				 accelerationVec.add( walkAcceleration, 0 );
			}
			
			if (Gdx.input.isKeyPressed(Keys.W)) {
				if (velocityY == 0) {
					jump();
				}
				 
			}
			
			velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );
			
			velocityVec.x = MathUtils.clamp( velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed );
			
			moveBy( velocityVec.x * dt, velocityVec.y * dt );
			accelerationVec.set(0,0);
			
			belowSensor.setPosition(getX() + 4, getY() - 8);
			
			if (tipo.equalsIgnoreCase("base")) {
				
				if (velocityVec.y < -900) {
					velocityVec.y = -900;
				}
				
				jumpSpeed = 500;
				gravity = 800;
				if (this.isOnSolid()) {
					belowSensor.setColor(Color.GREEN);
					
					if (velocityVec.x <= 20 && velocityVec.x >= -20 ) {
						setAnimation(quieto);
					} else {
						setAnimation(fluir);
					}
				} else {
					accelerationVec.add(0, -gravity);
					belowSensor.setColor(Color.RED);
					
					setAnimation(jump);
				}
			} else if (tipo.equalsIgnoreCase("fire")) {
				
				if (velocityVec.y < -900) {
					velocityVec.y = -900;
				}
				
				jumpSpeed = 500;
				gravity = 800;
				if (this.isOnSolid()) {
					belowSensor.setColor(Color.GREEN);
					
					if (velocityVec.x <= 20 && velocityVec.x >= -20 ) {
						setAnimation(quietoFire);
					} else {
						setAnimation(fluirFire);
					}
				} else {
					accelerationVec.add(0, -gravity);
					belowSensor.setColor(Color.RED);
					
					setAnimation(jumpFire);
				}
			} else if (tipo.equalsIgnoreCase("green")) {
				
				if (velocityVec.y < -900) {
					velocityVec.y = -900;
				}
				
				jumpSpeed = 800;
				gravity = 800;
				if (this.isOnSolid()) {
					belowSensor.setColor(Color.GREEN);
					
					if (velocityVec.x <= 20 && velocityVec.x >= -20 ) {
						setAnimation(quietoJumper);
					} else {
						setAnimation(fluirJumper);
					}
				} else {
					accelerationVec.add(0, -gravity);
					belowSensor.setColor(Color.RED);
					
					setAnimation(jumpJumper);
				}
			} else if (tipo.equalsIgnoreCase("yellow")) {
				
				if (velocityVec.y < -300) {
					velocityVec.y = -300;
				}
				
				jumpSpeed = 500;
				gravity = 500;
				if (this.isOnSolid()) {
					belowSensor.setColor(Color.GREEN);
					
					if (velocityVec.x <= 20 && velocityVec.x >= -20 ) {
						setAnimation(quietoBubble);
					} else {
						setAnimation(fluirBubble);
					}
				} else {
					accelerationVec.add(0, -gravity);
					belowSensor.setColor(Color.RED);
					
					setAnimation(jumpBubble);
				}
			}
				
			if ( velocityVec.x > 0 ) {
				setScaleX(1);
			}
			if ( velocityVec.x < 0 ) {
				setScaleX(-1);
			}
			colisonInicial();
			
			velocityY = velocityVec.y;
			
			if (velocityY < 0) {
				colisionando = false;
			}
			
			alignCameraEjeXFijo();
			boundToWorld();
		}
	}
	
	/**
	 * MEtdo para comprobar si el sensor esta colisionando con el objetoq ue se le pasa
	 * @param actor
	 * @return
	 */
	public boolean belowOverlaps(BaseActor actor) {
        return belowSensor.overlaps(actor);
    }
	
	/**
	 * Metodo para detectar cuando colisiona con el suelo, para pintar una marca de pintura
	 */
	public void colisonInicial() {		
		if (isOnSolid() && !colisionando) {
			if (tipo.equalsIgnoreCase("fire")) {
				new ManchaDePintura(this.getX()-70, this.getY()-70, getStage(), "red");
				if (velocityY == -900) {
					new PuntuacionesEnPantalla(this.getX(), this.getY(), getStage(), -50, "red");
					extraPuntuation -= 50;
				} else {
					new PuntuacionesEnPantalla(this.getX(), this.getY(), getStage(), -10, "red");
					extraPuntuation -= 10;
				}
			} else if (tipo.equalsIgnoreCase("green")) {
				new ManchaDePintura(this.getX()-70, this.getY()-70, getStage(), "green");
				if (velocityY == -900) {
					new PuntuacionesEnPantalla(this.getX(), this.getY(), getStage(), -50, "green");
					extraPuntuation -= 50;
				} else {
					new PuntuacionesEnPantalla(this.getX(), this.getY(), getStage(), -10, "green");
					extraPuntuation -= 10;
				}
			} else if (tipo.equalsIgnoreCase("yellow")) {
				new ManchaDePintura(this.getX()-70, this.getY()-70, getStage(), "yellow");
				new PuntuacionesEnPantalla(this.getX(), this.getY(), getStage(), -10, "yellow");
				extraPuntuation -= 50;
			}
			soundPintura.play(soundsVolume);
			colisionando = true;
		}
	}
	
	/**
	 * Metodo para comprobar si esta sobre un objeto solido o plataforma
	 * @return
	 */
	public boolean isOnSolid() {
		for (BaseActor actor : BaseActor.getList(getStage(), Plataforma.class.getName())) {
			Plataforma plataforma = (Plataforma)actor;
			if (belowOverlaps(plataforma) && plataforma.isEnable()) {
				if (velocityVec.y < 0 && !plataforma.getName().equalsIgnoreCase("Trampolin") 
						&& !plataforma.getName().equalsIgnoreCase("Teleport")) {
					velocityVec.y = 0;
				}
				return true;
			}
		}
		
		for (BaseActor actor : BaseActor.getList(getStage(), Solid.class.getName())) {
			Solid solid = (Solid)actor;
			if (belowOverlaps(solid) && solid.isEnable()) {
				if (velocityVec.y < 0) {
					velocityVec.y = 0;
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Metodo para saltar, si tiene el power up verde el sonido es diferente
	 */
	public void jump() {
		if(isOnSolid()) {
			velocityVec.y = jumpSpeed;
			colisionando = false;
			
			if (!this.tipo.equalsIgnoreCase("green")) {
				soundJump.play(soundsVolume);
			} else {
				soundJumpGreen.play(soundsVolume);
			}
			
			
		}
	}
	
	/**
	 * Metodo para comprobar si el player esta callendo
	 * @return
	 */
	public boolean isFalling() {
        return (velocityVec.y < 0);
    }

	/**
	 * Metodo para rebotar y reproducir el sonido pertinente
	 */
    public void rebotar() {
    	colisionando = false;
        velocityVec.y = 1.5f * jumpSpeed;
        soundRebotar.play(soundsVolume);
    }

    /**
     * Metodo para comprobar si el player esta saltando
     * @return
     */
    public boolean isJumping() {
    	return (velocityVec.y > 0); 
    }
    
    /**
     * Metodo de muerte, ejecuta la animacion y hace desaparecer al player
     */
    public void dead() {
    	setAnimation(dead);
    	belowSensor.remove();
    	vivo = false;
    	addAction(Actions.fadeOut(1));
    	addAction(Actions.after(Actions.removeActor()));
    }

    /**
     * Metodo getter para obtener el tipo del player segun el power upp obtenido
     * @return
     */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Metodo setter para darle un tipo alplayer sengun el power upp obtenido
	 * @param tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Metodo getter para obtener la velocidad sobre el eje Y
	 * @return
	 */
	public float getVelocityY() {
		return velocityY;
	}

	/**
	 * Metodo setter para darleuna velocidad en el eje Y
	 * @param velocityY
	 */
	public void setVelocityY(float velocityY) {
		this.velocityY = velocityY;
	}

	/**
	 * Metodo getter para obtener la puntuacion extra
	 * @return
	 */
	public int getExtraPuntuation() {
		return extraPuntuation;
	}

	/**
	 * Metodo setter pra sarle una puntuacion extra
	 * @param extraPuntuation
	 */
	public void setExtraPuntuation(int extraPuntuation) {
		this.extraPuntuation = extraPuntuation;
	} 
	
	
	
	
	

}
