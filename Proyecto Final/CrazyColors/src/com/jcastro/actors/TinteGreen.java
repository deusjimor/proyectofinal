package com.jcastro.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase TinteGreen
 * @author Jimor
 *
 */
public class TinteGreen extends Coleccionables{
	
	private Animation basico;

	/**
	 * Constructor de la clase tinte green, determina un tipo de animacion dependiendo el tipo de mapa
	 * le da una hitbox y un color
	 * @param x
	 * @param y
	 * @param s
	 * @param tipo
	 */
	public TinteGreen(float x, float y, Stage s, String tipo) {
		super(x, y, s);
		setName("TinteGreen");
		
		if (tipo.equalsIgnoreCase("normal")) {
			basico = loadAnimationFromSheet("assets/images/coleccionables/gota.png", 1, 4, 0.1f, true);
		} else if (tipo.equalsIgnoreCase("neon")) {
			basico = loadAnimationFromSheet("assets/images/coleccionables/energySphere.png", 1, 3, 0.1f, true);
			setScale(0.6f, 0.6f);
		}

		setBoundaryPolygon(8);
		
		setColor(Color.GREEN);
		
	}

}
