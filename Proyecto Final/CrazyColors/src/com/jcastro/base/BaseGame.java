package com.jcastro.base;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Metodo BaseGame
 * @author Jimor
 *
 */
public abstract class BaseGame extends Game{
	public static LabelStyle labelStyle;
	
	private static BaseGame game;
	
	/**
	 * Metodo create de la clase BaseGame
	 */
	public void create() {
		labelStyle = new LabelStyle();
		
		FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("assets/text/OpenSans.ttf"));
		
		FreeTypeFontParameter fontParameter = new FreeTypeFontParameter();
		fontParameter.size = 20;
		fontParameter.color = Color.WHITE;
		fontParameter.borderWidth = 2;
		fontParameter.borderColor = Color.BLACK;
		fontParameter.borderStraight = true;
		fontParameter.minFilter = TextureFilter.Linear;
		fontParameter.magFilter = TextureFilter.Linear;
		
		BitmapFont cusFont = fontGenerator.generateFont(fontParameter);
		labelStyle.font = cusFont;
	}

	/**
	 * Constructor de la clase BaseGame
	 */
	public BaseGame() {
		game = this;
	}
	
	/**
	 * Metodo setter para retornar la screen activa
	 * @param s
	 */
	public static void setActiveScreen(BaseScreen s) {
		game.setScreen(s);
	}
	
	

}
