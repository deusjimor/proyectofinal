package com.jcastro.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase BaseScreen
 * @author Jimor
 *
 */
public abstract class BaseScreen implements Screen, InputProcessor{
	
	protected Stage mainStage;
	protected Stage effectStage;
	protected Stage uiStage;
	
	public BaseScreen() {
		this.mainStage = new Stage();
		this.effectStage = new Stage();
		this.uiStage = new Stage();
		
		initialize();
	}

	protected abstract void initialize();
	
	public abstract void update(float dt);
	
	/**
	 * Metodo render de la clase BaseScreen
	 */
	public void render(float dt) {
		uiStage.act(dt);
		effectStage.act(dt);
		mainStage.act(dt);
		
		update(dt);
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		mainStage.draw();
		effectStage.draw();
		uiStage.draw();
	}
	
	public void resize(int width, int height) {
		
	}
	
	public void pause() {
		
	}
	
	public void resume() {
		
	}
	
	public void dispose() {
		
	}
	
	public void show() {
	
	}
	
	public void hide() {
	
	}
	
	public boolean keyDown(int keycode) {
		return false;
	}
	
	public boolean keyUp(int keycode) { 
		return false;
	}
	
	public boolean keyTyped(char c) { 
		return false;
	}
	
	public boolean mouseMoved(int screenX, int screenY) { 
		return false;
	}
	
	public boolean scrolled(int amount) { 
		return false;
	}
	
	public boolean touchDown(int screenX, int screenY, int pointer, int button) { 
		return false;
	}
	
	public boolean touchDragged(int screenX, int screenY, int pointer) { 
		return false;
	}
	
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	
	
	
	
	

}
