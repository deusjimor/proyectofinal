package com.jcastro.game;

import com.jcastro.base.BaseGame;
import com.jcastro.windows.Menu;

/**
 * Clase Game
 * @author Jimor
 *
 */
public class Game extends BaseGame{
	
	/**
	 * Clase create de la clase Game, llama a la clase menu
	 */
	public void create() {
		super.create();
		setActiveScreen(new Menu());
	}

}
