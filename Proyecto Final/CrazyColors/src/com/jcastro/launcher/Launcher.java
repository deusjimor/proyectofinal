package com.jcastro.launcher;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.jcastro.game.Game;

/**
 * Clase Launcher
 * @author Jimor
 *
 */
public class Launcher {
	
	public static void main(String[] args) {
		new LwjglApplication(new Game(), "Crazy Colors", 960, 640);
	}

}
