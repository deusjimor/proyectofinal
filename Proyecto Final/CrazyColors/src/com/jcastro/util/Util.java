package com.jcastro.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
/**
 * Clase Util
 * @author Jimor
 *
 */
public class Util {
	
	public static float volumenControlMusic;
	public static float volumenControlEffects;
	
	/**
	 * Metodo para guardar las opciones de sonido del usuario
	 */
	public static void saveProperties(){
		try {
			OutputStream ouput = new FileOutputStream("assets/config/config.properties");
			
			Properties properties = new Properties();
			properties.setProperty("volumeMusic", String.valueOf(volumenControlMusic));
			properties.setProperty("volumeEffects", String.valueOf(volumenControlEffects));
			
			properties.store(ouput, null);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo para cargar las opciones de audio del usuario
	 */
	public static void loadProperties() {
		try {
			InputStream input = new FileInputStream("assets/config/config.properties");
			
			Properties properties = new Properties();
			properties.load(input);
			
			volumenControlMusic = Float.parseFloat(properties.getProperty("volumeMusic"));
			volumenControlEffects = Float.parseFloat(properties.getProperty("volumeEffects"));
			
		} catch (IOException e) {
			saveProperties();
		}
	}
}
