package com.jcastro.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.jcastro.base.BaseActor;
import com.jcastro.base.BaseScreen;
import com.jcastro.game.Game;
import com.jcastro.util.Util;

/**
 * Clase Menu
 * @author Jimor
 *
 */
public class Menu extends BaseScreen{
	private Stage stage;
	private boolean activo;
	public Music adventureTime;
	
	public static Menu sharedInstaced;
	
	public float musicVolume;

	/**
	 * Metodo initialize del menu, carga todos los botones iniciales necesarios(seleccion de nivel, opciones, salir),
	 * cargara su fondo y su musica correspondiente
	 */
	@Override
	protected void initialize() {
		sharedInstaced = this;
		
		Util.loadProperties();
		
		musicVolume = Util.volumenControlMusic;
		
		activo = true;
		Skin skin = new Skin(Gdx.files.internal("assets/ui/maincraftCo/craftacular-ui.json"));
		Skin skin2 = new Skin(Gdx.files.internal("assets/ui/skin/comic-ui.json"));

		adventureTime = (Music)Gdx.audio.newMusic(Gdx.files.internal("assets/music/soundtracks/Adventure Meme.mp3"));
		adventureTime.setLooping(true);
		adventureTime.setVolume(musicVolume);
		adventureTime.play();
		
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		BaseActor fondo = new BaseActor(0, 0, mainStage);
		fondo.loadTexture("assets/images/fondos/munuFondo.png");
		fondo.setSize(960, 640);
		
		//	Titulo
		BaseActor title = new BaseActor(0, 0, mainStage);
		title.loadTexture("assets/images/fondos/tituloMenu.png");
		title.setSize(960, 640);
		/*Label title = new Label("CRAZY COLORS", skin);
		title.setSize(200, 200);
		title.setColor(Color.PURPLE);
		title.setX(350);
		title.setY(500);*/
		
		TextButton botonJugar = new TextButton("Jugar", skin2);
		botonJugar.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				
				if (activo == true) {
					Game.setActiveScreen(new LevelSeleccion());
				
					activo = false;
				}
				
			}
		});
		
		botonJugar.setX(650);
		botonJugar.setY(300);
		
		//	Boton de opciones
		TextButton botonOpciones = new TextButton("Opciones", skin2);
		botonOpciones.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			
				if (activo == true) {
					Game.setActiveScreen(new OptionsMenu());
					activo = false;
				}
				
			}
		});
		
		botonOpciones.setX(650);
		botonOpciones.setY(200);
		
		//	Boton exit
		TextButton botonExit = new TextButton("Salir", skin2);
		botonExit.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				adventureTime.stop();
				if (activo == true) {
					Gdx.app.exit();
					activo = false;
				}
				
			}
		});
		
		botonExit.setX(650);
		botonExit.setY(100);
		botonExit.setColor(Color.RED);
		
		//	AddStages
		stage.addActor(fondo);
		stage.addActor(title);
		stage.addActor(botonJugar);
		stage.addActor(botonOpciones);
		stage.addActor(botonExit);
	}

	/**
	 * Metodo update del menu
	 */
	@Override
	public void update(float dt) {
		
	}
	
	/**
	 * Metodo render del menu
	 */
	@Override
	public void render(float dt) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
}
