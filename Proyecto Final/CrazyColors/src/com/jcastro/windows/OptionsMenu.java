package com.jcastro.windows;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jcastro.base.BaseActor;
import com.jcastro.base.BaseScreen;
import com.jcastro.game.Game;
import com.jcastro.util.Util;

/**
 * Clase OptionsMenu
 * @author Jimor
 *
 */
public class OptionsMenu extends BaseScreen{
	
	public static OptionsMenu sharedInstanced;

	private Stage stage;
	private boolean activo;
	
	public Slider sliderVolumeMusic;
	public Slider sliderVolumeEffects;
	
	/**
	 * Initialice del Menu de opciones, el menu de opciones cargara dos sliders para regular el nivel
	 * de la musica y de los efectos y guardara dichos cambios en un prefs personald el usuario, 
	 * el prefs se crea al iniciar el juego por primera vez con los ajustes de sonido base
	 */
	@Override
	protected void initialize() {
		sharedInstanced = this;
		activo = true;
		
		Skin skin = new Skin(Gdx.files.internal("assets/ui/maincraftCo/craftacular-ui.json"));
		Skin skin2 = new Skin(Gdx.files.internal("assets/ui/skin/comic-ui.json"));
		
		Util.loadProperties();
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		//	Background
		BaseActor fondo = new BaseActor(0, 0, mainStage);
		fondo.loadTexture("assets/images/fondos/munuFondo.png");
		fondo.setSize(960, 640);
		
		//	Title
		Label title = new Label("VOLUME", skin);
		title.setSize(250, 250);
		title.setColor(Color.WHITE);
		title.setX(100);
		title.setY(400);
		
		Label musicTitle = new Label("Music", skin);
		musicTitle.setSize(200, 200);
		musicTitle.setColor(Color.WHITE);
		musicTitle.setX(100);
		musicTitle.setY(325);
		
		Label musicEffects = new Label("Effects", skin);
		musicEffects.setSize(200, 200);
		musicEffects.setColor(Color.WHITE);
		musicEffects.setX(100);
		musicEffects.setY(225);
		
		//	Sliders
		sliderVolumeMusic = new Slider(0f, 10f, 1, false, skin);
		sliderVolumeMusic.setPosition(250, 400);
		sliderVolumeMusic.setSize(200, 50);
		sliderVolumeMusic.setValue(Util.volumenControlMusic * 10);
		
		sliderVolumeEffects = new Slider(0f, 10f, 1, false, skin);
		sliderVolumeEffects.setPosition(250, 300);
		sliderVolumeEffects.setSize(200, 50);
		sliderVolumeEffects.setValue(Util.volumenControlEffects * 10);
		
		//	Exit button
		TextButton exit = new TextButton("<", skin2);
		exit.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				Menu.sharedInstaced.adventureTime.stop();
				// save en properties
				Util.volumenControlMusic = sliderVolumeMusic.getValue()/10;
				Util.volumenControlEffects = sliderVolumeEffects.getValue()/10;
				Util.saveProperties();
				if (activo) {
					Game.setActiveScreen(new Menu());
					activo = false;
				}
			}
		});
			
		exit.setX(850);
		exit.setY(30);
		exit.setSize(60, 60);
		exit.setColor(Color.RED);
		
		//	AddStages
		stage.addActor(fondo);
		stage.addActor(title);
		stage.addActor(musicTitle);
		stage.addActor(musicEffects);
		stage.addActor(sliderVolumeMusic);
		stage.addActor(sliderVolumeEffects);
		stage.addActor(exit);
		
	}

	/**
	 * Metodo update de la clase del menu de opciones
	 */
	@Override
	public void update(float dt) {
		
	}

	/**
	 * Render del menu de opciones
	 */
	public void render(float dt) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

}
