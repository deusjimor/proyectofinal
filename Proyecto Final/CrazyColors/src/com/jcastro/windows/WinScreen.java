package com.jcastro.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jcastro.base.BaseActor;
import com.jcastro.base.BaseGame;
import com.jcastro.base.BaseScreen;
import com.jcastro.game.Game;
import com.jcastro.util.Util;

/**
 * Clase WinScreen
 * @author Jimor
 *
 */
public class WinScreen extends BaseScreen{
	private Stage stage;
	private boolean activo;
	
	private int endPoints;
	private Label visualPoints;
	
	private TextButton exit;
	
	public Music nanoHoedown;
	
	public static WinScreen sharedInstanced;
	
	public float musicVolume;
	
	public WinScreen(int puntuacion) {
		endPoints = puntuacion;
	}

	/**
	 * Metodo initialize de la pantalla de victoria, cargara una label mostrando las puntuaciones
	 * y su fondo correspondiente, asi como la muica de dicha pantalla
	 */
	@Override
	protected void initialize() {
		sharedInstanced = this;
		
		musicVolume = Util.volumenControlMusic;
		
		activo = true;
		
		nanoHoedown = (Music)Gdx.audio.newMusic(Gdx.files.internal("assets/music/soundtracks/Nano Hoedown.mp3"));
		nanoHoedown.setLooping(true);
		nanoHoedown.setVolume(musicVolume);
		nanoHoedown.play();
		
		Skin skin = new Skin(Gdx.files.internal("assets/ui/maincraftCo/craftacular-ui.json"));
		Skin skin2 = new Skin(Gdx.files.internal("assets/ui/skin/comic-ui.json"));
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		//	Titulo
		BaseActor titulo = new BaseActor(0, 0, mainStage);
		titulo.loadTexture("assets/images/fondos/PuntuacionTitle.png");
		titulo.setX(180);
		titulo.setY(450);
		
		//	Fondo
		BaseActor fondo = new BaseActor(0, 0, mainStage);
		fondo.loadAnimationFromSheet("assets/images/fondos/winSheetFondo.png", 1, 2, 0.1f, true);
		fondo.setSize(960, 640);
		
		//	Puntuacion
		visualPoints = new Label("", BaseGame.labelStyle);
		visualPoints.setColor(Color.WHITE);
		visualPoints.setPosition(390, 330);
		uiStage.addActor(visualPoints);
		
		//	Botones
		exit = new TextButton("<", skin2);
		exit.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				nanoHoedown.stop();
				Menu.sharedInstaced.adventureTime.play();
				if (activo == true) {
					Game.setActiveScreen(new LevelSeleccion());
					activo = false;
				}
			}
		});
			
		exit.setX(850);
		exit.setY(30);
		exit.setSize(60, 60);
		exit.setColor(Color.RED);
		
		//	AddStages
		stage.addActor(fondo);
		stage.addActor(titulo);
		stage.addActor(visualPoints);
		stage.addActor(exit);
		
	}

	/**
	 * Metodo update de la ventna de victoria
	 */
	@Override
	public void update(float dt) {
		
		

	}
	
	/**
	 * Metodo render de la ventana de victoria
	 */
	public void render(float dt) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
		visualPoints.setText("Puntuacion: " + endPoints);
	}

}
